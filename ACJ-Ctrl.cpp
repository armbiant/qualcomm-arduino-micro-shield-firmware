//
// ACJ Controller software
//
// Temperature controller for Bacho ACJ heatingsystem.
// System requirements:
//   - Processormodule:   Genuino Micro
//   - Signalinterface:   ACJ Shield for Genuino rev.1
//
// SPDX-License-Identifier: MIT
//
#include <Arduino.h>
#include <Arduino_FreeRTOS.h> // https://github.com/feilipu/Arduino_FreeRTOS_Library
#include <task.h>
#include "pin_defs.h"

// define FreeRTOS Tasks
extern void powerController(void* parameters);

void setup() {
	// initialize serial communication at 9600 bits per second:
	Serial.begin(9600);	// @suppress("Avoid magic numbers")
	delay(1000);		// @suppress("Avoid magic numbers")
	// print the header (using tab separated columns)
	Serial.println("ACJ_Controller software");
	Serial.println("RoomTemp [degC]\tDesiredTemp [degC]\tHeating power [kW]\tRelayOn");

	//set I/O pin directions
	pinMode(triac1_pin, OUTPUT);
	pinMode(triac2_pin, OUTPUT);
	pinMode(triac3_pin, OUTPUT);
	pinMode(relay_pin, OUTPUT);
	pinMode(triac_status_pin, OUTPUT);

	xTaskCreate(powerController, "PowerCtrl", 420, NULL, 1, NULL); // @suppress("Avoid magic numbers")

}

void loop() {
	// Empty. Things are done in Tasks.
}
