# Firmware for Arduino Micro shield

## Description
This project contains the firmware for the [PCB hardware project](https://gitlab.com/acj-ctrl/hw/arduino-micro-shield).   
The software is based on the [Arduino software framework](https://www.arduino.cc/en/software), and the provided file can be opened, compiled and flashed onto the PCB using the Arduino IDE.  
The source code uses the [PID Library](https://github.com/br3ttb/Arduino-PID-Library), which is very detailed described in [this article](http://brettbeauregard.com/blog/2011/04/improving-the-beginners-pid-introduction/).  

## License
This work is licensed under a MIT License.

## Project status
The first version has been working fine for years, but is completely "standalone".    
I've now started some work that aims to [make it communicate with an external computer](https://gitlab.com/acj-ctrl/sw/arduino-micro-shield-fw/-/milestones/1), to listen for the target temperature and report back temperature and power. This would make then it possible to integrate it into home automation systems.
