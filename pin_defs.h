// pin_defs.h
//
// Defines the pin usage on the Arduino

#ifndef PIN_DEFS_H_
#define PIN_DEFS_H_

//pin number constants
const int room_temp_sensor_pin = A1; //the input pin for the room temperature sensor
const int potentiometer_pin = A0;    //the input pin for the potentiometer (temperature setpoint)

const int triac1_pin = 9;        //the output pin for the triac controlling the heater on phase L1
const int triac2_pin = 10;       //the output pin for the triac controlling the heater on phase L2
const int triac3_pin = 11;       //the output pin for the triac controlling the heater on phase L3
const int relay_pin = 12;        //the output pin for the fixed 3kW relay
const int triac_status_pin = 13; // select the pin for the statusLED

#endif /* PIN_DEFS_H_ */
